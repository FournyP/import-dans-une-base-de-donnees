import unittest, os, shutil, sqlite3
from app import Verif_existance_SIV, Verif_Valid_File, csv_to_list, CSV_compatible, insert_list, update_list, update_or_insert

class TestImport(unittest.TestCase):

    def setUp(self):
        """
        Création de la BD
        """
        self.connection = sqlite3.connect('tests.sqlite3')

    def tearDown(self):
        """
        Suppréssion de la BD
        """
        self.connection.close()
        os.remove('tests.sqlite3')

    def test_Verif_Valid_CSV_File(self):
        self.assertEqual(Verif_Valid_File("data.csv"), 1)
        self.assertEqual(Verif_Valid_File("data.txt"), 0)

    def test_existance_SIV(self):
        cursor = self.connection.cursor()
        result = cursor.execute("SELECT name FROM sqlite_master WHERE type='table';").fetchall()
        desired_result = ['SIV']
        #La base ne contient pas la table SIV
        self.assertListEqual(result, [])
        #self.assertFalse(all(elem in result for elem in desired_result))
        
        Verif_existance_SIV(self.connection)
        result = cursor.execute("SELECT name FROM sqlite_master WHERE type='table';").fetchone()
        self.assertListEqual(list(result), desired_result)
        #self.assertTrue(all(elem in result for elem in desired_result))
        cursor.close()
        
    def test_CSV_compatible(self):
        cursor = self.connection.cursor()
        Verif_existance_SIV(self.connection)
        self.assertFalse(CSV_compatible('tests/falsetest.csv'))
        self.assertTrue(CSV_compatible('tests/test.csv'))
        cursor.close()
    
    def test_update_list(self):
        cursor = self.connection.cursor()
        Verif_existance_SIV(self.connection)

        #Ajout d'une ligne dans la table SIV
        cursor.execute("INSERT INTO SIV VALUES ('3822 Omar Square Suite 257 Port, OK 43251', 'Smith', 'Jerome', 'OVC-568', '03/05/2012', '9780082351764', 'Williams Inc', 'Enhanced well-modulated moderator', 'LightGoldenRodYellow', '45-1743376', '34-7904216', '3462', '37578077', '32', '3827', '110', 'Inc', '92-3625175', '79266482')")
        result = cursor.execute("SELECT COUNT(*) FROM SIV").fetchone()
        self.assertEqual(1, result[0])

        #On met la ligne à jour (avec les même données)
        list_data = csv_to_list('tests/test.csv')
        update_list(list_data[0], self.connection)

        #On check ensuite si aucune ligne n'à été ajouté
        result = cursor.execute("SELECT COUNT(*) FROM SIV").fetchone()
        self.assertEqual(1, result[0])
        cursor.close()

    def test_insert_list(self):
        cursor = self.connection.cursor()
        Verif_existance_SIV(self.connection)

        #Ajout d'une ligne dans la table SIV
        cursor.execute("INSERT INTO SIV VALUES ('3822 Omar Square Suite 257 Port, OK 43251', 'Smith', 'Jerome', 'OVC-568', '03/05/2012', '9780082351764', 'Williams Inc', 'Enhanced well-modulated moderator', 'LightGoldenRodYellow', '45-1743376', '34-7904216', '3462', '37578077', '32', '3827', '110', 'Inc', '92-3625175', '79266482')")
        result = cursor.execute("SELECT COUNT(*) FROM SIV").fetchone()
        self.assertEqual(1, result[0])

        #On insert une nouvelle row
        list_data = csv_to_list('tests/test.csv')
        insert_list(list_data[0], self.connection)
        
        #On check ensuite si la ligne a bien été ajouté
        result = cursor.execute("SELECT COUNT(*) FROM SIV").fetchone()
        self.assertEqual(result[0], 2)
        cursor.close()

    def test_update_or_insert(self):
        cursor = self.connection.cursor()
        Verif_existance_SIV(self.connection)
        #On check s'il y a rien dans la table SIV
        result = cursor.execute("SELECT COUNT(*) FROM SIV").fetchone()
        self.assertEqual(0, result[0])

        #Ajout d'une première ligne dans la table SIV
        #Ligne correspondant à la ligne présente dans test.csv
        list_data = csv_to_list('tests/test.csv')
        update_or_insert(list_data[0], self.connection)
        result = cursor.execute("SELECT COUNT(*) FROM SIV").fetchone()
        #On check qu'il a bien insert
        self.assertEqual(1, result[0])

        #On insert dans la DB
        insert_list(list_data[0], con)

        #Puis on check si il retourne ensuite 'update' vu qu'on vient de l'insérer
        update_or_insert(list[0], self.connection)
        result = cursor.execute("SELECT COUNT(*) FROM SIV").fetchone()
        #On check qu'il a bien update
        self.assertEqual(1, result[0])

        cursor.close()

    def test_csv_to_list(self):
        list_data = csv_to_list('tests/test.csv')
        self.assertListEqual(['3822 Omar Square Suite 257 Port Emily, OK 43251', 'Smith', 'Jerome', 'OVC-568', '03/05/2012', '9780082351764', 'Williams Inc', 'Enhanced well-modulated moderator', 'LightGoldenRodYellow', '45-1743376', '34-7904216', '3462', '37578077', '32', '3827', '110', 'Inc', ' 92-3625175', ' 79266482'], list_data[0])