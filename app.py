import csv
import sys
import sqlite3
import logging 
from datetime import datetime
from time import strftime

logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s',filename="Log.log",level=logging.DEBUG)

def Verif_Valid_File(file):
    if file[-3:] == 'csv' :
        return 1
    else:
        return 0

def csv_to_list(file):
    list_data = []
    compteur = 0
    with open(file,'r') as csvfile:   
        reader=csv.reader(csvfile,delimiter=';')
        for lines in reader:
            if compteur != 0:
                list_data.append(lines)
            compteur = compteur + 1
    return list_data

def CSV_compatible(file):
    result_desired = ['adresse_titulaire', 'nom', 'prenom', 'immatriculation', 'date_immatriculation', 'vin', 'marque', 'denomination_commerciale', 'couleur', 'carrosserie', 'categorie', 'cylindree', 'energie', 'places', 'poids', 'puissance', 'type', 'variante', 'version']
    with open(file,'r') as csvfile:   
        reader=csv.reader(csvfile,delimiter=';')
        if result_desired in reader:
            return True
        else:
            logging.error('CSV incompatible')
            return False

def Verif_existance_SIV(con):
    cursor = con.cursor()
    result = cursor.execute("SELECT name FROM sqlite_master WHERE type='table';").fetchone()
    if result is None or 'SIV' not in result:
        cursor.execute("CREATE TABLE SIV (adresse_titulaire, nom, prenom, immatriculation, date_immatriculation, vin, marque, denomination_commerciale, couleur, carrosserie, categorie, cylindree, energie, places, poids, puissance, type, variante, version);")
        logging.info('Création de la table SIV')
    cursor.close()
    con.commit()

def insert_list(list_data, con):
    cursor = con.cursor()
    cursor.execute("INSERT INTO SIV VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);", list_data)
    logging.info('Insertion de la plaque %s dans la table SIV', list_data[3])
    cursor.close()
    con.commit()

def move_immat_at_end(list_data):
    immat = list_data.pop(3)
    list_data.append(immat)
    return list_data

def update_list(list_data, con):
    cursor = con.cursor()
    list_data = move_immat_at_end(list_data)
    cursor.execute("UPDATE SIV set adresse_titulaire = ?, nom = ?, prenom = ?, date_immatriculation = ?, vin = ?, marque = ?, denomination_commerciale = ?, couleur = ?, carrosserie = ?, categorie = ?, cylindree = ?, energie = ?, places = ?, poids = ?, puissance = ?, type = ?, variante = ?, version = ? WHERE immatriculation = ?;", list_data)
    logging.info("Mise à jour de la plaque %s dans la table SIV", list_data[18])
    cursor.close()
    con.commit()

def update_or_insert(list_data, con):
    cursor = con.cursor()
    result = cursor.execute("SELECT COUNT(*) FROM SIV WHERE immatriculation = ?;", (list_data[3],)).fetchone()
    if result[0] == 0:
        insert_list(list_data, con)
    else:
        update_list(list_data, con)
    cursor.close()
    con.commit()

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print("Veuillez saisir un fichier et le chemin vers la DB")
        logging.error("Saisie des paramètres invalide")
        sys.exit(3)
    else:
        data = csv_to_list(sys.argv[1])
        path = "%s.sqlite3"%(sys.argv[2])
        con = sqlite3.connect(path)
        Verif_existance_SIV(con)
        for line in data:
            update_or_insert(line, con)